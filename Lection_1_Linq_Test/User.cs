using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace Lection_1_Linq_Test
{
    public class User
    {
        
        [Required] 
        [JsonPropertyName("id")] 
        public int id { get; set; }
        
        [Required] 
        [JsonPropertyName("firstName")] 
        public string firstName { get; set; }
        
        [Required] 
        [JsonPropertyName("lastName")] 
        public string lastName { get; set; }
        
        [Required] 
        [JsonPropertyName("email")] 
        public string email { get; set; }
        
        [Required] 
        [JsonPropertyName("birthday")] 
        public DateTime birthday { get; set; }
        
        [Required] 
        [JsonPropertyName("registeredAt")] 
        public DateTime registeredAt { get; set; }
        
        [Required] 
        [JsonPropertyName("teamId")] 
        public int? teamId { get; set; }

        public Team UserTeams { get; set; }
        

        public override string ToString()
        {
            return $"Id: {id}\n" +
                   $"FirstName: {firstName}\n" +
                   $"LastName: {lastName}\n" +
                   $"Email: {email}\n" +
                   $"Birthday: {birthday}\n" +
                   $"RegisteredAt: {registeredAt}\n" +
                   $"TeamId: {teamId}";
        }
        
    }
}