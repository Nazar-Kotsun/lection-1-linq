namespace Lection_1_Linq_Test
{
    public class TaskDTO
    {
        public int id { get; set; }
        public string name { get; set; }

        public override string ToString()
        {
            return $"Id: {id}\nName: {name}";
        }
    }
}