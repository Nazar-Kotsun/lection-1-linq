using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using System.Collections.Generic;

namespace Lection_1_Linq_Test
{
    public class Team
    {
        [Required] 
        [JsonPropertyName("id")] 
        public int id { get; set; }

        [Required] 
        [JsonPropertyName("name")] 
        public string name { get; set; }

        [Required] 
        [JsonPropertyName("createdAt")] 
        public DateTime createdAt { get; set; }

        public List<User> Users { get; set; }

        public override string ToString()
        {
            return $"Id: {id}\n" +
                   $"Name: {name}\n" +
                   $"CreatedAt: {createdAt}";
        }
    }
}