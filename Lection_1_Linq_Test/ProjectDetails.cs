namespace Lection_1_Linq_Test
{
    public class ProjectDetails
    {
        public Project project { get; set; }
        public Task theLongesDescriptionOfTask { get; set; }
        public Task theShortedNameOfTask { get; set; }
        public int? CountOfUsers { get; set; }
    }
}