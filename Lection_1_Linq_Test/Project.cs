using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using System;
using System.Collections.Generic;

namespace Lection_1_Linq_Test
{
    public class Project
    {
        [Required] 
        [JsonPropertyName("id")] 
        public int id { get; set; }

        [Required] 
        [JsonPropertyName("name")] 
        public string name { get; set; }
        
        [Required] 
        [JsonPropertyName("description")] 
        public string description { get; set; }
        
        [Required] 
        [JsonPropertyName("createdAt")] 
        public DateTime createdAt { get; set; }
        
        [Required] 
        [JsonPropertyName("deadline")] 
        public DateTime deadline { get; set; }
        
        [Required] 
        [JsonPropertyName("authorId")] 
        public int authorId { get; set; }
        
        [Required] 
        [JsonPropertyName("teamId")] 
        public int teamId { get; set; }

        public List<Task> Tasks { get; set; }
        public User author { get; set; }
        public Team? team { get; set; }

        public override string ToString()
        {
            return $"Id: {id}\n" +
                   $"Name: {name}\n" +
                   $"Description: {description}\n" +
                   $"CreatedAt: {createdAt}\n" +
                   $"Deadline: {deadline}\n" +
                   $"AuthorId: {authorId}\n" +
                   $"TeamId: {teamId}";
        }
    }
}

