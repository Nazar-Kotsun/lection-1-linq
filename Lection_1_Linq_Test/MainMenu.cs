using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;

namespace Lection_1_Linq_Test
{
    public class MainMenu
    {
        private IEnumerable<Project> projects;
        private IEnumerable<User> users;
        private IEnumerable<Task> tasks;
        private IEnumerable<Team> teams;

        private bool exit = false;
        private int choice;
        private HttpClientRequest request = new HttpClientRequest();

        public MainMenu()
        {
            Initialization();
        }
        private void Initialization()
        {
            projects = request.GetProjects();
            users = request.GetUsers();
            tasks = request.GetTasks();
            teams = request.GetTeams();

            teams = teams.GroupJoin(users,
                team => team.id,
                user => user.teamId,
                (team, userss) => new Team
                {
                    id = team.id,
                    name = team.name,
                    createdAt = team.createdAt,
                    Users = userss.ToList()
                });

            var joinUserAndTeamAndTasks = users.Join(teams,
                user => user.teamId,
                team => team.id,
                (user, team) => new User()
                {
                    id = user.id,
                    firstName = user.firstName,
                    lastName = user.lastName,
                    email = user.email,
                    birthday = user.birthday,
                    registeredAt = user.registeredAt,
                    teamId = team.id,
                    UserTeams = team
                }
            ).Join(tasks,
                user => user.id,
                task => task.performerId,
                (user, task) => new Task()
                {
                    id = task.id,
                    name = task.name,
                    description = task.description,
                    state = task.state,
                    createdAt = task.createdAt,
                    finishedAt = task.finishedAt,
                    projectId = task.projectId,
                    performerId = user.id,
                    Performer = user
                }
            );

            projects = projects.GroupJoin(joinUserAndTeamAndTasks,
                project => project.id,
                tasksInner => tasksInner.projectId,
                (project, tasksInner) => new Project()
                {
                    id = project.id,
                    name = project.name,
                    description = project.description,
                    createdAt = project.createdAt,
                    deadline = project.deadline,
                    authorId = project.authorId,
                    // author = SearchUser(userTest, project.authorId),
                    author = users.First(user => user.id == project.authorId),
                    teamId = project.teamId,
                    // team = SearchTeam(teamTest, project.teamId),
                    team = teams.First(team => team.id == project.teamId),
                    Tasks = tasksInner.Where(t => t.projectId == project.id).ToList()
                }
            );
        }

        public void DrawSeparatorLine(string text)
        {
            Console.WriteLine("------------" + text + "------------");
        }
        private void StopProgramToContinue()
        {
            Console.Write("Please enter any key to continue...");
            Console.ReadKey();
            Console.Clear();
        }
        public void StartMenu()
        {
            while (!exit)
            {
                ShowMenu();
            }
        }

        private void ShowMenu()
        {   //Отримати список користувачів за алфавітом first_name
            //(по зростанню) з відсортованими tasks по довжині name (за спаданням).
            Console.WriteLine("1 - Get dictionary(key - project; value - count of tasks) -> (task_1)");
            Console.WriteLine("2 - Get the list of tasks where name is less than 45 characters -> (task_2)");
            Console.WriteLine("3 - Get the list (id,name) of the tasks that ended in 2020 -> (task_3)");
            Console.WriteLine("4 - Get the list (id, name of team, list users) of the teams whose members\n" +
                              "are older than 10 year -> (task_4)");
            Console.WriteLine("5 - Get the list of users alphabetically with sorted tasks by length \n "
                              +"  of name -> (task_5)");
            Console.WriteLine("6 - Get more detailed information about user -> (task_6)");
            Console.WriteLine("7 - Get more detailed information about project -> (task_7)");
            Console.WriteLine("0 - Exit");
            DoChoice();
        }

        private void CheckCorrectEnteringInteger(out int value)
        {
            while (!int.TryParse(Console.ReadLine(), out value))
            {
                Console.WriteLine("Incorrect entering!");
            }
        }

        private void DoChoice()
        {
            Console.Write("Your choice: ");
            // CheckCorrectEnteringInteger(out choice);
            int.TryParse(Console.ReadLine(), out choice);

            switch (choice)
            {
                case 1:
                {
                    CheckGetTaskCountByUserId();
                    break;
                }
                case 2:
                {
                    CheckGetListOfTasksWhereNameOfTaskLess45CharactersByUserId();
                    break;
                }
                case 3:
                {
                    CheckGetAllTaskDTOByUserId();
                    break;
                }
                case 4:
                {
                    CheckGetAllTeamsDTO();
                    break;
                }
                case 5:
                {
                    CheckGetListUsersSorted();
                    break;
                }
                case 6:
                {
                    CheckGetDetailInformationByUserId();
                    break;
                }
                case 7:
                {
                    CheckGetDetailInformationByProjectIdO();
                    break;
                }
                case 0:
                {
                    Exit();
                    break;
                }

            }

        }
        
        private Dictionary<Project,int> GetTaskCountByUserId(int id)
        {
            
            return projects.Where(p => p.authorId == id)
                .ToDictionary(p => p, p => p.Tasks.Count());
        }

        private void CheckGetTaskCountByUserId()
        {
            int id;
            Console.Write("Please, enter user id: ");
            CheckCorrectEnteringInteger(out id);

            Dictionary<Project,int > dictionary = GetTaskCountByUserId(id);
            if (dictionary.Count != 0)
            {
                foreach (var pair in dictionary)
                {
                    DrawSeparatorLine("Key-Project");
                    Console.WriteLine(pair.Key);
                    DrawSeparatorLine("Value-Count");
                    Console.WriteLine(pair.Value);
                }
                DrawSeparatorLine("-----------");
            }
            else
            {
                Console.WriteLine("This user doesn't have any project!");
            }
            StopProgramToContinue();
        }
        
        private List<Task> GetListOfTasksWhereNameOfTaskLess45CharactersByUserId(int id)
        {
            return projects.SelectMany(project => project.Tasks)
                .Where(task => task.performerId == id && task.name.Length < 45).ToList();
        }

        private void CheckGetListOfTasksWhereNameOfTaskLess45CharactersByUserId()
        {
            Console.Write("Please enter user id: ");
            int id;
            CheckCorrectEnteringInteger(out id);
            List<Task> listTasks = GetListOfTasksWhereNameOfTaskLess45CharactersByUserId(id);
            Console.WriteLine("Number of tasks: " + listTasks.Count);
            foreach (var task in listTasks)
            {
                DrawSeparatorLine("Task");
                Console.WriteLine(task);
            }
            StopProgramToContinue();
        }
        
        private List<TaskDTO> GetAllTaskDTOByUserId(int id)
        {
            return projects.SelectMany(project => project.Tasks)
                .Where(task => task.performerId == id && task.finishedAt.Year == DateTime.Now.Year)
                .Select(t => new TaskDTO() {id = t.id, name = t.name}).ToList();
        }
        private void CheckGetAllTaskDTOByUserId()
        {
            Console.Write("Please enter user id: ");
            int id;
            CheckCorrectEnteringInteger(out id);
            List<TaskDTO> listTasksDTO = GetAllTaskDTOByUserId(id);
            Console.WriteLine("Number of tasksDTO: " + listTasksDTO.Count);
            foreach (var task in listTasksDTO)
            {
                DrawSeparatorLine("Task");
                Console.WriteLine(task);
            }
            StopProgramToContinue();
        }
        
        private List<TeamDTO> GetAllTeamsDTO()
        {
            return teams.GroupJoin(users,
                team => team.id,
                user => user.teamId,
                (team, user) => new TeamDTO()
                {
                    id = team.id,
                    name = team.name,
                    Users = user.Where(user => DateTime.Now.Year - user.birthday.Year > 10)
                        .OrderByDescending(user => user.registeredAt).ToList()
                }
            ).ToList();
        }

        private void CheckGetAllTeamsDTO()
        {
            List<TeamDTO> teamsDto = GetAllTeamsDTO();
            foreach (var team in teamsDto)
            {
                DrawSeparatorLine("Team");
                Console.WriteLine("Id: " + team.id);
                Console.WriteLine("Name: " + team.name);
                foreach (var user in team.Users)
                {
                    DrawSeparatorLine("User");
                    Console.WriteLine(user);
                }
            }
            
            StopProgramToContinue();
        }
        private List<UserTasks> GetListUsersSorted()
        {
            return users.GroupJoin(tasks,
                user => user.id,
                task => task.performerId,
                (user, task) => new UserTasks()
                {
                    user = user,
                    userTasks = task.Where(t => t.performerId == user.id)
                        .OrderByDescending(t => t.name.Length).ToList()

                }).OrderBy(user => user.user.firstName).ToList();
        }

        private void CheckGetListUsersSorted()
        {
            List<UserTasks> usersSorted = GetListUsersSorted();
            Console.WriteLine("Number of user: " + usersSorted.Count);
            foreach (var valueUser in usersSorted)
            {
                DrawSeparatorLine("User");
                Console.WriteLine(valueUser.user);
                foreach (var valueTask in valueUser.userTasks)
                {
                    DrawSeparatorLine("Task");
                    Console.WriteLine(valueTask);
                }
            }
            StopProgramToContinue();
        }
        private UserDetails GetDetailInformationByUserId(int id)
        {
            UserDetails tmp = null;
            try{
                tmp = new UserDetails{
                    user = users.First(u => u.id == id),
                    lastProject = projects.Where(project => project.authorId == id)
                        .OrderByDescending(project => project.createdAt).First(),
                    CountOfTasks = projects.Where(project => project.authorId == id)
                        .OrderByDescending(project => project.createdAt).First().Tasks.Count,
                    CountOfStartedOrCanceledProject = tasks
                        .Where(task => task.performerId == id &&
                                       (task.state == TaskState.Started || task.state == TaskState.Canceled))
                        .Count(),
                    TheLongestTask = tasks.Where(task => task.performerId == id)
                        .OrderByDescending(task => task.finishedAt - task.createdAt)
                        .First()
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine("This user doesn't have any projects");
            }
            return tmp;
        }

        private void CheckGetDetailInformationByUserId()
        {
            Console.Write("Please enter user id: ");
            int id;
            CheckCorrectEnteringInteger(out id);
            UserDetails userDetails = GetDetailInformationByUserId(id);
            if (userDetails != null)
            {
                DrawSeparatorLine("User");
                Console.WriteLine(userDetails.user);
                DrawSeparatorLine("Last project");
                Console.WriteLine(userDetails.lastProject);
                DrawSeparatorLine("Count of tasks");
                Console.WriteLine(userDetails.CountOfTasks);
                DrawSeparatorLine("Count of started or canceled projects");
                Console.WriteLine(userDetails.CountOfStartedOrCanceledProject);
                DrawSeparatorLine("The longest task");
                Console.WriteLine(userDetails.TheLongestTask);
            }
        }
        private List<ProjectDetails> GetDetailInformationByProjectIdO()
        {
            return projects.SelectMany(project => project.Tasks,
        (projectInner, task) => new ProjectDetails()
            {
                project = projectInner,
                theLongesDescriptionOfTask = projectInner.Tasks
                    .OrderByDescending(task => task.description.Length)
                    .First(),
                theShortedNameOfTask = projectInner.Tasks
                    .OrderBy(task => task.name.Length)
                    .First(),
                CountOfUsers = projects
                    .Where(project => project.id == projectInner.id && 
                                      (project.description.Length > 20 || project.Tasks.Count < 3))
                    .First().team.Users.Count
            }).ToList();
        }
        private void CheckGetDetailInformationByProjectIdO()
        {
            List<ProjectDetails> projects = GetDetailInformationByProjectIdO();
            foreach (var projectValue in projects)
            {
                DrawSeparatorLine("Project");
                Console.WriteLine(projectValue.project);
                DrawSeparatorLine("The longest description of task");
                Console.WriteLine(projectValue.theLongesDescriptionOfTask);
                DrawSeparatorLine("The shorted name of task");
                Console.WriteLine(projectValue.theShortedNameOfTask);
                DrawSeparatorLine("Count of users in team");
                Console.WriteLine(projectValue.CountOfUsers);
            }
            StopProgramToContinue();
        }
        private void Exit()
        {
            exit = true;
        }
        
    }
}