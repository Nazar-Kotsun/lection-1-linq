using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using System;

namespace Lection_1_Linq_Test
{
    public class Task
    {
        [Required] 
        [JsonPropertyName("id")] 
        public int id { get; set; }

        [Required] 
        [JsonPropertyName("name")] 
        public string name { get; set; }
        
        [Required] 
        [JsonPropertyName("description")] 
        public string description { get; set; }
        
        [Required] 
        [JsonPropertyName("state")] 
        public TaskState state { get; set; }
        
        [Required] 
        [JsonPropertyName("createdAt")] 
        public DateTime createdAt { get; set; }
        
        [Required] 
        [JsonPropertyName("finishedAt")] 
        public DateTime finishedAt { get; set; }
        
        [Required] 
        [JsonPropertyName("projectId")] 
        public int projectId { get; set; }
        
        [Required] 
        [JsonPropertyName("performerId")] 
        public int performerId { get; set; }

        public User Performer { get; set; }

        public override string ToString()
        {
            return $"Id: {id}\n" +
                   $"Name: {name}\n" +
                   $"Description: {description}\n" +
                   $"State: {state}\n" +
                   $"CreatedAt: {createdAt}\n" +
                   $"FinishedAt: {finishedAt}\n" +
                   $"ProjectId: {projectId}\n" +
                   $"PerformerId: {performerId}";
        }
        
    }
}
