namespace Lection_1_Linq_Test
{
    public class UserDetails
    {
        public User user { get; set; }
        public Project lastProject { get; set; }
        public int CountOfTasks { get; set; }
        public int CountOfStartedOrCanceledProject { get; set; }
        public Task TheLongestTask { get; set; }

        public override string ToString()
        {
            return $"User: \n{user}\n" +
                   $"Last project: \n{lastProject}\n" +
                   $"Count of tasks: {CountOfTasks}\n" +
                   $"Count of started or canceled projects: {CountOfStartedOrCanceledProject}\n" +
                   $"The longest task: {TheLongestTask}";
        }
    } 
}

