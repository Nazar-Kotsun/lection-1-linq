using System.Collections.Generic;
using System.Net.Http;
using System.Text.Json;
using System;

namespace Lection_1_Linq_Test
{
    public class HttpClientRequest
    {
        private HttpClient client;
        private HttpResponseMessage responseMessage;
        
        public HttpClientRequest()
        {
            HttpClientHandler httpClientHandler = new HttpClientHandler();
            httpClientHandler.ServerCertificateCustomValidationCallback = (httpRequestMessage, cert, cetChain, policyErrors) => true; client = new HttpClient(httpClientHandler);
            client.BaseAddress = new Uri("https://bsa20.azurewebsites.net/api/");
        }
        
        public IEnumerable<Project> GetProjects()
        {
            responseMessage = client.GetAsync("Projects").Result;
            return JsonSerializer.Deserialize<IEnumerable<Project>>(responseMessage.Content.ReadAsStringAsync().Result);
        }

        public Project GetProjectById(int id)
        {
            responseMessage = client.GetAsync("Projects" + "/" + id).Result;
            return JsonSerializer.Deserialize<Project>(responseMessage.Content.ReadAsStringAsync().Result);
        }
        
        public IEnumerable<User> GetUsers()
        {
            responseMessage = client.GetAsync("Users").Result;
            return JsonSerializer.Deserialize<IEnumerable<User>>(responseMessage.Content.ReadAsStringAsync().Result);
        }

        public User GetUserById(int id)
        {
            responseMessage = client.GetAsync("Users" + "/" + id).Result;
            return JsonSerializer.Deserialize<User>(responseMessage.Content.ReadAsStringAsync().Result);
        }
        
        public IEnumerable<Team> GetTeams()
        {
            responseMessage = client.GetAsync("Teams").Result;
            return JsonSerializer.Deserialize<IEnumerable<Team>>(responseMessage.Content.ReadAsStringAsync().Result);
        }

        public Team GetTeamById(int id)
        {
            responseMessage = client.GetAsync("Teams" + "/" + id).Result;
            return JsonSerializer.Deserialize<Team>(responseMessage.Content.ReadAsStringAsync().Result);
        }
        
        public IEnumerable<Task> GetTasks()
        {
            responseMessage = client.GetAsync("Tasks").Result;
            return JsonSerializer.Deserialize<IEnumerable<Task>>(responseMessage.Content.ReadAsStringAsync().Result);
        }

        public Task GetTaskById(int id)
        {
            responseMessage = client.GetAsync("Tasks" + "/" + id).Result;
            return JsonSerializer.Deserialize<Task>(responseMessage.Content.ReadAsStringAsync().Result);
        }

        public IEnumerable<TaskStateModel> GetTaskStates()
        {
            responseMessage = client.GetAsync("TaskStates" ).Result;
            return JsonSerializer.Deserialize<IEnumerable<TaskStateModel>>(responseMessage.Content.ReadAsStringAsync().Result);
        }
    }
}