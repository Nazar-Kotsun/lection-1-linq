using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace Lection_1_Linq_Test
{
    public class TaskStateModel
    {
        [Required] 
        [JsonPropertyName("id")] 
        public int id { get; set; }
        
        [Required] 
        [JsonPropertyName("value")] 
        public int value { get; set; }
        
        public override string ToString()
        {
            return $"Id: {id}\n" +
                   $"Value: {value}\n";
        }
    }
}