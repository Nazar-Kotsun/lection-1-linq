namespace Lection_1_Linq_Test
{
    public enum TaskState
    {
        Created, Started, Finished, Canceled
    }
}
