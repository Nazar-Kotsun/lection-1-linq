using System.Collections.Generic;

namespace Lection_1_Linq_Test
{
    public class TeamDTO
    {
        public int? id { get; set; }
        public string name { get; set; }
        public List<User> Users { get; set; }
    }
}