using System.Collections.Generic;

namespace Lection_1_Linq_Test
{
    public class UserTasks
    {
        public User user { get; set; }
        public List<Task> userTasks { get; set; }
        
    }
}